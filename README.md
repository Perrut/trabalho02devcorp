# A branch `trabalho04` deste projeto se refere ao quarto trabalho da disciplina

### Trabalho 02

URL's do projeto:

- http://localhost:8080/trabalho02 -> Lista todos os eventos e edições
- http://localhost:8080/trabalho02/buscar -> Buscar evento com edições por nome
- http://localhost:8080/trabalho02/eventos -> Lista todos os eventos
- http://localhost:8080/trabalho02/eventos/novo -> Adicionar novo evento
- http://localhost:8080/trabalho02/eventos/editar -> Editar evento
- http://localhost:8080/trabalho02/eventos/excluir?id=idevento -> Excluir evento por id
- http://localhost:8080/trabalho02/edicoes -> Lista todas as edições de eventos
- http://localhost:8080/trabalho02/edicoes/novo -> Adicionar nova edição de evento
- http://localhost:8080/trabalho02/edicoes/editar -> Editar edição de evento
- http://localhost:8080/trabalho02/edicoes/excluir?id=idedição -> Excluir edição de evento por id

Projeto feito no Eclipse com servidor Tomcat, já há uma base de dados remota então não há necessidade de configuração de base local.
