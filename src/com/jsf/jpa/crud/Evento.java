package com.jsf.jpa.crud;
 
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name="evento")
public class Evento {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id; 
    private String nome;
    private String sigla;
    private String areaConcentracao;
    private String instituicaoOrganizadora;
    
    @OneToMany(mappedBy="evento", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Edicao> edicoes;
 
    public Evento() { }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getAreaConcentracao() {
		return areaConcentracao;
	}

	public void setAreaConcentracao(String areaConcentracao) {
		this.areaConcentracao = areaConcentracao;
	}

	public String getInstituicaoOrganizadora() {
		return instituicaoOrganizadora;
	}

	public void setInstituicaoOrganizadora(String instituicaoOrganizadora) {
		this.instituicaoOrganizadora = instituicaoOrganizadora;
	}

	public List<Edicao> getEdicoes() {
		if(Objects.isNull(this.edicoes)) {
			this.edicoes = new ArrayList<>();
		}
		return edicoes;
	}

	public void setEdicoes(List<Edicao> edicoes) {
		this.edicoes = edicoes;
	}
}