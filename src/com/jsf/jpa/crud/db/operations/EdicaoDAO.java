package com.jsf.jpa.crud.db.operations;
 
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.jsf.jpa.crud.Edicao;

@SuppressWarnings("unchecked")
public class EdicaoDAO {
 
    private static final String PERSISTENCE_UNIT_NAME = "JSFJPACrud";   
    private static EntityManager entityMgrObj = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME).createEntityManager();
    private static EntityTransaction transactionObj = entityMgrObj.getTransaction();
 
    public static List<Edicao> getAllEdicoes() {
        Query queryObj = entityMgrObj.createQuery("SELECT e FROM Edicao e");
		List<Edicao> edicaoList = queryObj.getResultList();
        if (edicaoList != null && edicaoList.size() > 0) {           
            return edicaoList;
        } else {
            return new ArrayList<>();
        }
    }
 
    public static void createNewEdicao(Edicao e) {
        if(!transactionObj.isActive()) {
            transactionObj.begin();
        }
 
        entityMgrObj.persist(e);
        transactionObj.commit();
    }
    
    public static void updateEdicao(Edicao e) {
    	if(!transactionObj.isActive()) {
            transactionObj.begin();
        }
    	
		Edicao ed = entityMgrObj.find(Edicao.class, e.getId());
		ed.setEvento(e.getEvento());
		ed.setNumero(e.getNumero());
		ed.setAno(e.getAno());
		ed.setDataInicio(e.getDataInicio());
		ed.setDataFim(e.getDataFim());
		ed.setCidade(e.getCidade());
		ed.setPais(e.getPais());
		entityMgrObj.persist(ed);        
    	
        transactionObj.commit();
    }
    
    public static Edicao getEdicaoById(int edicaoId) {
    	Edicao ed = entityMgrObj.find(Edicao.class, edicaoId);
    	
    	return ed;
    }
 
    public static void deleteEdicao(int edicaoId) {
        if (!transactionObj.isActive()) {
            transactionObj.begin();
        }
 
        Edicao deleteEdicaoObj = new Edicao();
        if(isEdicaoIdPresent(edicaoId)) {
        	deleteEdicaoObj.setId(edicaoId);
            entityMgrObj.remove(entityMgrObj.merge(deleteEdicaoObj));
        }       
        transactionObj.commit();
    }
 
    private static boolean isEdicaoIdPresent(int edicaoId) {
        boolean idResult = false;
        Query queryObj = entityMgrObj.createQuery("SELECT e FROM Edicao e WHERE e.id = :id");
        queryObj.setParameter("id", edicaoId);
        Edicao selectedEventoId = (Edicao) queryObj.getSingleResult();
        if(selectedEventoId != null) {
            idResult = true;
        }
        return idResult;
    }
}