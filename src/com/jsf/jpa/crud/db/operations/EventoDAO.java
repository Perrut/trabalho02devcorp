package com.jsf.jpa.crud.db.operations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.jsf.jpa.crud.Evento;

@SuppressWarnings("unchecked")
public class EventoDAO {

	private static final String PERSISTENCE_UNIT_NAME = "JSFJPACrud";
	private static EntityManager entityMgrObj = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME)
			.createEntityManager();
	private static EntityTransaction transactionObj = entityMgrObj.getTransaction();

	public static List<Evento> getAllEventos() {
		Query queryObj = entityMgrObj.createQuery("SELECT e FROM Evento e");
		List<Evento> eventoList = queryObj.getResultList();
		if (eventoList != null && eventoList.size() > 0) {
			return eventoList;
		} else {
			return new ArrayList<>();
		}
	}

	public static void createNewEvento(Evento e) {
		if (!transactionObj.isActive()) {
			transactionObj.begin();
		}

		entityMgrObj.persist(e);
		transactionObj.commit();
	}

	public static void updateEvento(Evento e) {
		if (!transactionObj.isActive()) {
			transactionObj.begin();
		}

		Evento ev = entityMgrObj.find(Evento.class, e.getId());
		ev.setAreaConcentracao(e.getAreaConcentracao());
		ev.setInstituicaoOrganizadora(e.getInstituicaoOrganizadora());
		ev.setNome(e.getNome());
		ev.setSigla(e.getSigla());
		entityMgrObj.persist(ev);

		transactionObj.commit();
	}

	public static Evento getEventoById(int eventoId) {
		Evento ev = entityMgrObj.find(Evento.class, eventoId);

		return ev;
	}

	public static List<Evento> buscarPorNome(String nome) {
		Query queryObj = entityMgrObj
				.createQuery("SELECT e FROM Evento e where upper(e.nome) like '%" + nome.toUpperCase() + "%'");
		List<Evento> eventoList = queryObj.getResultList();

		if (eventoList != null && eventoList.size() > 0) {
			return eventoList;
		} else {
			return new ArrayList<>();
		}
	}

	public static void deleteEvento(int eventoId) {
		if (!transactionObj.isActive()) {
			transactionObj.begin();
		}

		Evento deleteEventoObj = getEventoById(eventoId);
		entityMgrObj.remove(deleteEventoObj);

		transactionObj.commit();
	}
}