package trabalho02;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jsf.jpa.crud.Evento;
import com.jsf.jpa.crud.db.operations.EventoDAO;

public class EditarEvento extends HttpServlet {
	private static final long serialVersionUID = 3296253606648631984L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Integer eventoId = Integer.parseInt(request.getParameter("id"));
		Evento evento = EventoDAO.getEventoById(eventoId);
		
		response.setContentType("text/html;charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>\n"
				+ "<title>Editar evento</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "	<h1>Preencha os campos com as informações adequadas:</h1>\n"
				+ "	<form action =\"editar\" method=\"post\">\n"
				+ "	\n"
				+ "<input type=\"hidden\" name=\"id\" value=\""+evento.getId()+"\" >"
				+ "	Nome: <input type=\"text\" name=\"nome\" value=\""+evento.getNome()+"\" />\n"
				+ "	<br />\n"
				+ "	Área de concentração: <input type=\"text\" name=\"areaconcentracao\" value=\""+evento.getAreaConcentracao()+"\" />\n"
				+ "	<br />\n"
				+ "	Instituição organizadora: <input type=\"text\" name=\"instituicaoorganizadora\" value=\""+evento.getInstituicaoOrganizadora()+"\" />\n"
				+ "	<br />\n"
				+ "	Sigla: <input type=\"text\" name=\"sigla\" value=\""+evento.getSigla()+"\" />\n"
				+ "	<br />\n"
				+ "	<input type=\"submit\" value=\"Salvar\" />\n"
				+ "	</form>\n"
				+ "     <p><a href=\"/trabalho02/eventos\">Eventos</a></p>\n"
				+ "</body>\n"
				+ "</html>");
		
		out.close();
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Evento e = new Evento();
		e.setId(Integer.parseInt(request.getParameter("id")));
		e.setAreaConcentracao(request.getParameter("areaconcentracao"));
		e.setInstituicaoOrganizadora(request.getParameter("instituicaoorganizadora"));
		e.setNome(request.getParameter("nome"));
		e.setSigla(request.getParameter("sigla"));
		
		EventoDAO.updateEvento(e);
		
		response.sendRedirect(request.getContextPath() + "/eventos");
	}
}