package trabalho02;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jsf.jpa.crud.db.operations.EventoDAO;

public class ExcluirEvento extends HttpServlet {
	private static final long serialVersionUID = 3296253606648631984L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=UTF-8");
		
		Integer eventoId = Integer.parseInt(request.getParameter("id"));
		EventoDAO.deleteEvento(eventoId);
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>\n"
				+ "<title>Eventos</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "	<div style=\"display: block; width: 500px; height: 500px; text-align: center\">\n"
				+ "		<p>Evento excluído</p>\n"
				+ "     <p><a href=\"/trabalho02/eventos\">Eventos</a></p>\n"
				+ "	</div>\n"
				+ "</body>\n"
				+ "</html>");
		
		out.close();
	}
}