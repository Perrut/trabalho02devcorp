package trabalho02;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jsf.jpa.crud.Edicao;
import com.jsf.jpa.crud.Evento;
import com.jsf.jpa.crud.db.operations.EdicaoDAO;
import com.jsf.jpa.crud.db.operations.EventoDAO;

public class EditarEdicao extends HttpServlet {
	private static final long serialVersionUID = 3296253606648631984L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Integer edicaoId = Integer.parseInt(request.getParameter("id"));
		Edicao edicao = EdicaoDAO.getEdicaoById(edicaoId);
		
		System.out.println(edicao.getDataFim());
		
		response.setContentType("text/html;charset=UTF-8");
		
		List<Evento> eventos = EventoDAO.getAllEventos();
		String eventosList = "";
		
		for(Evento evento : eventos) {
			if(evento.getId() == edicao.getEvento().getId()) {
				eventosList += "<option value=\""+evento.getId()+"\" selected>"+evento.getNome()+"</option>\n";
			} else {
				eventosList += "<option value=\""+evento.getId()+"\">"+evento.getNome()+"</option>\n";
			}
		}
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>\n"
				+ "<title>Atualizar edição</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "	<h1>Preencha os campos com as informações adequadas:</h1>\n"
				+ "	<form action =\"editar\" method=\"post\">\n"
				+ "	\n"
				+ "<input type=\"hidden\" name=\"id\" value=\""+edicao.getId()+"\" >"
				+ "	Ano: <input type=\"number\" name=\"ano\" value=\""+edicao.getAno()+"\" />\n"
				+ "	<br />\n"
				+ "	Cidade: <input type=\"text\" name=\"cidade\" value=\""+edicao.getCidade()+"\" />\n"
				+ "	<br />\n"
				+ "	Data início: <input type=\"date\" name=\"dataInicio\" value=\""+new SimpleDateFormat("yyyy-MM-dd").format(edicao.getDataInicio())+"\" />\n"
				+ "	<br />\n"
				+ "	Data fim: <input type=\"date\" name=\"dataFim\" value=\""+new SimpleDateFormat("yyyy-MM-dd").format(edicao.getDataFim())+"\" />\n"
				+ "	<br />\n"
				+ "	Número: <input type=\"number\" name=\"numero\" value=\""+edicao.getNumero()+"\" />\n"
				+ "	<br />\n"
				+ "	País: <input type=\"text\" name=\"pais\" value=\""+edicao.getPais()+"\" />\n"
				+ "	<br />\n"
				+ " <label for=\"evento\">Evento:</label>\n"
				+ "	<select name=\"evento\" id=\"evento\">\n"
				+ eventosList
				+ "	</select>"
				+ "	<input type=\"submit\" value=\"Salvar\" />\n"
				+ "	</form>\n"
				+ "     <p><a href=\"/trabalho02/edicoes\">Edições</a></p>\n"
				+ "</body>\n"
				+ "</html>");
		
		out.close();
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Edicao e = new Edicao();
		e.setId(Integer.parseInt(request.getParameter("id")));
		e.setAno(Integer.parseInt(request.getParameter("ano")));
		e.setCidade(request.getParameter("cidade"));
		try {
			e.setDataFim(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dataFim")));
		} catch (ParseException ex) {
			throw new RuntimeException(ex);
		}
		try {
			e.setDataInicio(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dataInicio")));
		} catch (ParseException ex) {
			throw new RuntimeException(ex);
		}
		e.setEvento(EventoDAO.getEventoById(Integer.parseInt(request.getParameter("evento"))));
		e.setNumero(Integer.parseInt(request.getParameter("numero")));
		e.setPais(request.getParameter("pais"));
		
		EdicaoDAO.updateEdicao(e);
		
		response.sendRedirect(request.getContextPath() + "/edicoes");
	}
}