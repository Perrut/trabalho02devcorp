package trabalho02;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jsf.jpa.crud.Edicao;
import com.jsf.jpa.crud.db.operations.EdicaoDAO;

public class ListarEdicoes extends HttpServlet {
	private static final long serialVersionUID = 3296253606648631984L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<Edicao> edicoes = EdicaoDAO.getAllEdicoes();
		String edicoesList = "";
		
		for(Edicao edicao : edicoes) {
			edicoesList +=
					"<li> Evento: " + edicao.getEvento().getNome() + ", " + edicao.getNumero() + ", "
				  + edicao.getAno() + ", " + edicao.getDataInicio() + edicao.getDataFim() + ", "
				  + edicao.getCidade() + ", " + edicao.getPais()
				  + " <a href=\"/trabalho02/edicoes/editar?id="+edicao.getId()+"\">Editar</a> "
				  + "<a href=\"/trabalho02/edicoes/excluir"
				  + "?id=" + edicao.getId() +"\">Excluir</a>"
				  + "</li>\n";
		}
		
		
		response.setContentType("text/html;charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>\n"
				+ "<title>Listar edições</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "<h1>Edições</h1>\n"
				+ "<a href=\"/trabalho02\">Página Inicial</a>"
				+ "	<ul>\n"
				+ edicoesList
				+ "	</ul>\n"
				+ "<p><a href=\"/trabalho02/edicoes/novo\">Nova Edição</a></p>\n"
				+ "</body>\n"
				+ "</html>");
		
		out.close();
	}
}