package trabalho02;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jsf.jpa.crud.Evento;
import com.jsf.jpa.crud.db.operations.EventoDAO;

public class ListarEventos extends HttpServlet {
	private static final long serialVersionUID = 3296253606648631984L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<Evento> eventos = EventoDAO.getAllEventos();
		String eventosList = "";
		
		for(Evento evento : eventos) {
			eventosList +=
					"<li>" + evento.getNome() + ", " + evento.getAreaConcentracao() + ", "
				  + evento.getInstituicaoOrganizadora() + ", " + evento.getSigla()
				  + " <a href=\"/trabalho02/eventos/editar?id="+evento.getId()+"\">Editar</a> "
				  + "<a href=\"/trabalho02/eventos/excluir"
				  + "?id=" + evento.getId() +"\">Excluir</a>"
				  + "</li>\n";
		}
		
		
		response.setContentType("text/html;charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>\n"
				+ "<title>Listar eventos</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "<h1>Eventos</h1><br>"
				+ "<a href=\"/trabalho02\">Página Inicial</a>"
				+ "	<ul>\n"
				+ eventosList
				+ "</ul>\n"
				+ "<p><a href=\"/trabalho02/eventos/novo\">Novo Evento</a></p>\n"
				+ "</body>\n"
				+ "</html>");
		
		out.close();
	}
}