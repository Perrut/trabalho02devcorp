package trabalho02;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jsf.jpa.crud.Edicao;
import com.jsf.jpa.crud.Evento;
import com.jsf.jpa.crud.db.operations.EventoDAO;

public class BuscarEventos extends HttpServlet {
	private static final long serialVersionUID = 3296253606648631984L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>\n"
				+ "<title>Buscar eventos</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "<h1>Buscar eventos</h1>\n"
				+ "<a href=\"/trabalho02/eventos/novo\">Novo Evento</a><br>"
				+ "<a href=\"/trabalho02/edicoes/novo\">Nova Edição</a>"
				+ "	<form action=\"\" method=\"post\">\n"
				+ "		<label for=\"nome\">Nome do evento:</label> <input type=\"text\" id=\"nome\"\n"
				+ "			name=\"nome\">\n"
				+ "			\n"
				+ "		<input type=\"submit\" value=\"Buscar\"> \n"
				+ "	</form>"
				+ "</body>\n"
				+ "</html>");
		
		out.close();
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=UTF-8");
		
		List<Evento> eventos = EventoDAO.buscarPorNome(request.getParameter("nome"));
		String eventosStr = "";
		
		for(Evento evento : eventos) {
			eventosStr += "<ul>\n"
					+ "		<li>Nome: "+evento.getNome()+"</li>\n"
					+ "		<li>Sigla: "+evento.getSigla()+"</li>\n"
					+ "		<li>Área de Concentração: "+evento.getAreaConcentracao()+"</li>\n"
					+ "		<li>Instituição Organizadora: "+evento.getInstituicaoOrganizadora()+"</li>\n"
					+ listarEdicoes(evento)
					+ "	</ul>";
		}
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>\n"
				+ "<title>Buscar eventos</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "<h1>Buscar eventos</h1>\n"
				+ "	<form action=\"\" method=\"post\">\n"
				+ "		<label for=\"nome\">Nome do evento:</label> <input type=\"text\" id=\"nome\"\n"
				+ "			name=\"nome\" value=\""+request.getParameter("nome")+"\">\n"
				+ "			\n"
				+ "		<input type=\"submit\" value=\"Buscar\"> \n"
				+ "	</form>"
				+ eventosStr
				+ "</body>\n"
				+ "</html>");
		
		out.close();
	}
	
	private String listarEdicoes(Evento e) {
		String edicoesStr = "";
		
		if(e.getEdicoes().size() > 0) {
			edicoesStr += "		<li>Edições:</li>\n";
		}
		for(Edicao ed : e.getEdicoes()) {
			edicoesStr += "	<li>\n"
					+ "			<ul>\n"
					+ "				<li>Número: "+ed.getNumero()+"</li>\n"
					+ "				<li>Ano: "+ed.getAno()+"</li>\n"
					+ "				<li>Data de Início: "+ed.getDataInicio()+"</li>\n"
					+ "				<li>Data de Fim: "+ed.getDataFim()+"</li>\n"
					+ "				<li>Cidade: "+ed.getCidade()+"</li>\n"
					+ "				<li>País: "+ed.getPais()+"</li>\n"
					+ "			</ul>\n"
					+ "		</li>\n";
		}
		
		return edicoesStr;
	}
}